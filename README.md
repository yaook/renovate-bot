<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# Renovate-Bot

This repository contains the configuration to regularly run the renovate bot

## License

[Apache 2](LICENSE.txt)

## Test and run local

1. Build docker image: `docker build -t renovate-bot:master .`
2. Run renovate container:
   `docker run --rm -itu root renovate-bot:master bash`
3. Adjust /usr/src/app/config.js. `repositories` needs to point to the repo
   you want to test, `packageRules` can be adjusted to the rules you want to
   test. The `versioning` at repositories can be removed to run all managers.
   ```
       dryRun: "full",
   ...
       hostRules: [
         {
           hostType: 'docker',
           matchHost: 'registry.yaook.cloud', // 'process.env.CI_REGISTRY',
           //username: 'process.env.CI_REGISTRY_USER',
           //password: 'process.env.CI_REGISTRY_PASSWORD',
         },
       ],
       //autodiscover: true,
       repositories: [{repository: "yaook/images/ovn-bgp-agent", versioning: "regex"}],
       baseBranch: ["my/test-branche-at-image-repo"],
       "regexManagers": [
         {
           "fileMatch": ["^\\.gitlab-ci\\.yml$"],
           "matchStrings": [
             "datasource=(?<datasource>.*?) depName=(?<depName>.*?)( versioning=(?<versioning>.*?))?\\s.*?_VERSION: \"(?<currentValue>.*)\"\\s"
           ],
           "versioningTemplate": "{{#if versioning}}{{{versioning}}}{{else}}semver{{/if}}"
         }
       ],
       "packageRules": [
         {
           "matchPackageNames": ["registry.gitlab.com/yaook/images/ovn/ovn"],
           "versioning": "regex:^v(?<major>\\d+)(\\.(?<minor>\\d+))?(\\.(?<patch>\\d+))?(-(?<build>\\d+))(\\.(?<build_minor>\\d+))?(\\.(?<revision>\\d+))?$"
         }
       ],
   ```
4. Get gitlab token with this rights:

   **test-renovate write:** api, read_user, write_repository, read_registry

   **test-renovate:** read_api, read_user, read_repository, read_registry

   and export them:
   ```
   read -s RENOVATE_TOKEN
   export RENOVATE_TOKEN=$RENOVATE_TOKEN
   ```
5. May: Get github token. This is needed if you check github repos.

   A read-only token for public repositories is sufficient.

   ```
   read -s GITHUB_COM_TOKEN
   export GITHUB_COM_TOKEN=$GITHUB_COM_TOKEN
   ```
6. Run renovate: `renovate`

